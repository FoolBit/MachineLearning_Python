### 支持向量机/实践案例  


#### 代码

```
#导包
from __future__ import print_function
from time import time
#plt用来画图
import matplotlib.pyplot as plt
#train_test_split用来将数据集分割为训练集和测试集
from sklearn.model_selection import train_test_split
#fetch_lfw_people为人脸数据集
from sklearn.datasets import fetch_lfw_people
#GridSearchCV用于寻找合适的SVM参数组合
from sklearn.model_selection import GridSearchCV
#classfusion_report和confusion_matrix是后面用来给模型打分的
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
from sklearn.svm import SVC

# 如果数据集不在本地，下载数据集
lfw_people = fetch_lfw_people(min_faces_per_person=70, resize=0.4)
# 查看数据的维度（数量、高度、宽度），方便打印
n_samples, h, w = lfw_people.images.shape

#所有的训练数据,1288张图片，
X = lfw_people.data
#每张图片1850个特征值
n_features = X.shape[1]

# 对应的人脸标记,每一个人对应一个唯一的数字id
y = lfw_people.target

#人名
target_names = lfw_people.target_names
#人名数，对应人的个数
n_classes = target_names.shape[0]
#打印查看数据集的相关信息
print("Total dataset size:")
print("n_samples: %d\nn_features: %d\nn_classes: %d" % (n_samples, n_features, n_classes))

# 分割数据集和测试集，测试集占1/4
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)
#查看分割后的数据情况
print(X_train.shape, y_train.shape, X_test.shape, y_test.shape)

#PCA降维
#n_components:降维后的特征值数量
n_components = 150
print("Extracting the top %d eigenfaces from %d faces" % (n_components, X_train.shape[0]))
t0 = time()
#将高维的特征向量降低为低维的,建立模型
pca = PCA(svd_solver='randomized', n_components=n_components, whiten=True)
pca.fit(X, y)
print("done in %0.3fs" % (time() - t0))
eigenfaces = pca.components_.reshape((n_components, h, w))  # 三维
print("Projecting the input data on the eigenfaces orthonormal basis")

t0 = time()
#训练数据
X_train_pca = pca.transform(X_train)
#测试数据
X_test_pca = pca.transform(X_test)
print("done in %0.3fs" % (time() - t0))

# Train a SVM classification model
print("Fitting the classifier to the training set")
t0 = time()
#C 是对错误部分的惩罚；gamma 合成点
#param_grid = {'C': [1e3, 998, 1001, 999, 1002],'gamma': [0.0001, 0.003, 0.0035, 0.004, 0.0045], }
param_grid = {'C': [1e3, 5e3, 1e4, 5e4, 1e5],'gamma': [0.0001, 0.003, 0.0035, 0.004, 0.0045], }
#rbf处理图像较好，C和gamma组合，穷举出最好的一个组合
#clf = GridSearchCV(SVC(kernel='rbf', class_weight='auto'), param_grid)
clf = GridSearchCV(SVC(kernel='rbf', class_weight=None), param_grid)
#建模
clf = clf.fit(X_train_pca, y_train)
print("done in %0.3fs" % (time() - t0))
print("Best estimator found by grid search:")
#clf.best_estimator_可以看到这个模型的SVM的参数信息。
print(clf.best_estimator_)
# Quantitative evaluation of the model quality on the test set

print("Predicting people's names on the test set")
t0 = time()
y_pred = clf.predict(X_test_pca)
print("done in %0.3fs" % (time() - t0))

#打印预测结果报告
print(classification_report(y_test, y_pred, target_names=target_names))
#打印预测结果混淆矩阵
print(confusion_matrix(y_test, y_pred, labels=range(n_classes)))

# Qualitative evaluation of the predictions using matplotlib

#绘制数据图像
def plot_gallery(images, titles, h, w, n_row=3, n_col=4):
    plt.figure(figsize=(1.8 * n_col, 2.4 * n_row))
    plt.subplots_adjust(bottom=0, left=.01, right=.99, top=.90, hspace=.35)
    for i in range(n_row * n_col):
        plt.subplot(n_row, n_col, i + 1)
        plt.imshow(images[i].reshape((h, w)), cmap=plt.cm.gray)
        plt.title(titles[i], size=12)
        plt.xticks(())
        plt.yticks(())

# 打印预测结果
def title(y_pred, y_test, target_names, i):
    pred_name = target_names[y_pred[i]].rsplit(' ', 1)[-1]
    true_name = target_names[y_test[i]].rsplit(' ', 1)[-1]
    return 'predicted: %s\ntrue:      %s' % (pred_name, true_name)

prediction_titles = [title(y_pred, y_test, target_names, i) for i in range(y_pred.shape[0])]

plot_gallery(X_test, prediction_titles, h, w)
# 打印最重要的脸特征空间
eigenface_titles = ["eigenface %d" % i for i in range(eigenfaces.shape[0])]
plot_gallery(eigenfaces, eigenface_titles, h, w)
plt.show()
```

### 结果分析

打印数据集信息，直观了解数据信息
![图片](img\结果1.JPG) 

人脸识别率
![图片](img\结果2.JPG)  

识别结果展示
![图片](img\figure1.JPG)  

重要的脸部特征空间
![图片](img\figure2.JPG)
