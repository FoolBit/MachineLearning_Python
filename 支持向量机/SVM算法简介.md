## 支持向量机/算法简介

### 基本概念  

1. 支持向量机（support vector machines, SVM）是一种二分类模型，它的基本模型是定义在特征空间上的间隔 最大的线性分类器，间隔最大使它有别于感知机；SVM还包括核技巧，这使它成为实质上的非线性分类器。SVM的的学习策略就是间隔最大化，可形式化为一个求解凸二次规划的问题，也等价于正则化的合页损失函数的最小化问题。SVM的的学习算法就是求解凸二次规划的最优化算法。  
   
2. 函数间隔：对于给定的训练数据集$T$和超平面（$\omega$,$b$）,定义超平面（$\omega$,$b$）关于样本点（$x_i$,$y_i$）的函数间隔为
   $$\gamma_i=y_i(\omega\cdot{x_i}+b)$$  
定义超平面（$\omega$,$b$）关于训练数据集$T$的函数间隔为超平面（$\omega$,$b$）关于$T$中所有样本点$(x_i,y_i)$的函数间隔之最小值，即
$$\gamma=\min\limits_{i=1,2,...,N}\gamma_i$$  
3. 几何间隔：对于给定的训练数据集$T$和超平面（$\omega$,$b$）,定义超平面（$\omega$,$b$）关于样本点（$x_i$,$y_i$）的几何间隔为，
   $$\gamma_i=y_i(\frac{\omega}{\parallel\omega\parallel}\cdot{x_i}+\frac{b}{\parallel\omega\parallel})$$  
   定义超平面（$\omega$,$b$）关于训练数据集$T$的几何间隔为超平面（$\omega$,$b$）关于$T$中所有样本点（$x_i$,$y_i$）的几何间隔之最小值，即
   $$\gamma=\min_{i=1,2,...,N}\gamma_i$$
4. 分类  
>+ （1）线性可分支持向量机（linear support vector machine in linearly separable cse），又称硬间隔支持向量机  
>+ （2）线性支持向量机（linear support vector machine），又称软间隔支持向量机    
>+ （3）非线性支持向量机(non-linear support vector machine)  
5. 支持向量：在线性可分情况下，训练数据集的样本点中与分离超平面距离最近的样本点的实例称为支持向量
6. 凸优化问题：指约束最优化问题，
$$\min\limits_{\omega}{f(\omega)}$$
&emsp;&emsp;$s.t.$  
&emsp;&emsp;&emsp;&emsp;$g_i(\omega)\leq0, i=1,2,...,k$
&emsp;&emsp;&emsp;&emsp;$h_i(\omega)=0, i=1,2,...,l$
其中，目标函数$f(\omega)$和约束函数$g_i(\omega)$都是$R^n$上的连续可微的凸函数，约束函数$h_i(\omega)$是$R^n$上的<a href="https://baike.so.com/doc/489487-518297.html">仿射函数</a>。当目标函数$f(\omega)$和约束函数$g_i(\omega)$都是仿射函数时，上述凸优化问题成为凸二次规划问题。
7. 序列最小优化算法（sequential minimal optimization, SMO）是一种启发式算法，这种算法1998年由Platt提出。支持向量机的学习问题可以形式化为求解凸二次规划问题。这样的凸二次规划问题具有全局最优解，并且有许多最优化算法可以用于求解这一问题的解。但是当训练样本容量很大时，这些算法往往变得非常低效，以至无法使用。SMO算法就是为了高效地实现支持向量机学习问题。  

### 直观理解  
  

#### 分类问题

假设你的大学开设了一门机器学习（ML）课程。课程导师发现数学或统计学好的学生表现最佳。随着时间的推移，积累了一些数据，包括参加课程的学生的数学成绩和统计学成绩，以及在ML课程上的表现（使用两个标签描述，“优”、“差”）。  
现在，课程导师想要判定数学、统计学分数和ML课程表现之间的关系。也许，基于这一发现，可以指定参加课程的前提条件。  
这一问题如何求解？让我们从表示已有数据开始。我们可以绘制一张二维图形，其中一根轴表示数学成绩，另一根表示统计学成绩。这样每个学生就成了图上的一个点。  
点的颜色——绿或红——表示学生在ML课程上的词表现：“优”或“差”。
![图片](img/img1.jPG)
当一名学生申请加入课程时，会被要求提供数学成绩和统计学成绩。基于现有的数据，可以对学生在ML课程上的表现进行有根据的猜测。  
基本上我们想要的是某种“算法”，接受“评分元组”(math_score, stats_score)输入，预测学生在图中是红点还是绿点（绿/红也称为分类或标签）。当然，这一算法某种程度上包括了已有数据中的模式，已有数据也称为训练数据。  
在这个例子中，找到一条红聚类和绿聚类之间的直线，然后判断成绩元组位于线的哪一边，是一个好算法。  
![图片](img/img2.jpg)    
这里的直线是我们的分界（separating boundary）（因为它分离了标签）或者分类器（classifier）（我们使用它分类数据点）。上图显示了两种可能的分类器。


#### 好 vs 差的分类器  

这里有一个有趣的问题：上面的两条线都分开了红色聚类和绿色聚类。是否有很好的理由选择一条，不选择另一条呢？  
别忘了，分类器的价值不在于它多么擅长分离训练数据。我们最终想要用它分类未见数据点（称为测试数据）。因此，我们想要选择一条捕捉了训练集中的通用模式（general pattern）的线，这样的线在测试集上表现出色的几率很大。    
上面的第一条线看起来有点“歪斜”。下半部分看起来太接近红聚类，而上半部分则过于接近绿聚类。是的，它完美地分割了训练数据，但是如果它看到略微远离其聚类的测试数据点，它很有可能会弄错标签。    
第二条线没有这个问题。  
我们来看一个例子。下图中两个方形的测试数据点，两条线分配了不同的标签。显然，第二条线的分类更合理。  
![图片](img/img3.jpg)
第二条线在正确分割训练数据的前提下，尽可能地同时远离两个聚类。保持在两个聚类的正中间，让第二条线的“风险”更小，为每个分类的数据分布留出了一些摇动的空间，因而能在测试集上取得更好的概括性。  
SVM试图找到第二条线。上面我们通过可视化方法挑选了更好的分类器，但我们需要更准确一点地定义其中的理念，以便在一般情形下加以应用。下面是一个简化版本的SVM：  
找到正确分类训练数据的一组直线。
在找到的所有直线中，选择那条离最接近的数据点距离最远的直线。  
距离最接近的数据点称为支持向量（support vector）。支持向量定义的沿着分隔线的区域称为间隔（margin）。  
下图显示了之前的第二条线，以及相应的支持向量（黑边数据点）和间隔（阴影区域）。  
![图片](img/img4.jpg)
尽管上图显示的是直线和二维数据，SVM实际上适用于任何维度；在不同维度下，SVM寻找类似二维直线的东西。  
例如，在三维情形下，SVM寻找一个平面（plane），而在更高维度下，SVM寻找一个超平面（hyperplane）——二维直线和三维平面在任意维度上的推广。这也正是支持向量得名的由来。在高维下，数据点是多维向量，间隔的边界也是超平面。支持向量位于间隔的边缘，“支撑”起间隔边界超平面。  
可以被一条直线（更一般的，一个超平面）分割的数据称为线性可分（linearly separable）数据。超平面起到线性分类器（linear classifier）的作用。  
##### 允许误差  
在上一节中，我们查看的是简单的情形，完美的线性可分数据。然而，现实世界通常是乱糟糟的。你几乎总是会碰到一些线性分类器无法正确分类的实例。
下图就是一个例子。  
![图片](img/img5.jpg)
显然，如果我们使用一个线性分类器，我们将永远不能完美地分割数据点。我们同样不想干脆抛弃线性分类器，因为除了一些出轨数据点，线性分类器确实看起来很适合这个问题。  
SVM允许我们通过参数C指定愿意接受多少误差。C让我们可以指定以下两者的折衷：  
1. 较宽的间隔。    
2. 正确分类训练数据。C值较高，意味着训练数据上容许的误差较少。  
再重复一下，这是一个折衷。以间隔的宽度为代价得到训练数据上更好的分类。  
下图展示了随着C值的增加，分类器和间隔的变化（图中没有画出支持向量）：  
![图片](img/img6.jpg)  
上图中，随着C值的增加，分割线逐渐“翘起”。在高C值下，分割线试图容纳右下角大部分的红点。这大概不是我们想要的结果。而C=0.01的图像看起来更好的捕捉了一般的趋势，尽管和高C值情形相比，他在训练数据上的精确度较低。  
同时，别忘了这是折衷，注意间隔是如何随着C值的增加而收窄的。  
在上一节的例子中，间隔曾经是数据点的“无人区”。正如我们所见，这里再也无法同时得到良好的分割边界和相应的不包含数据点的间隔。总有一些数据点蔓延到了间隔地带。  
由于现实世界的数据几乎从来都不是整洁的，因此决定较优的C值很重要。我们通常使用交叉验证（cross-validation）之类的技术选定较优的C值。  
#### 非线性可分数据
我们已经看到，支持向量机有条不紊地处理完美线性可分或基本上线性可分的数据。但是，如果数据完全线性不可分，SVM的表现如何呢？毕竟，很多现实世界数据是线性不可分的。当然，寻找超平面没法奏效了。这看起来可不妙，因为SVM很擅长找超平面。  
下面是一个非线性可分数据的例子（这是知名的<a href="http://www.ece.utep.edu/research/webfuzzy/docs/kk-thesis/kk-thesis-html/node19.html">XOR数据集</a>的一个变体），其中的斜线是SVM找到的线性分类器：  
![图片](img/img7.jpg)  
显然这结果不能让人满意。我们需要做得更好。  
注意，关键的地方来了！我们已经有了一项非常擅长寻找超平面的技术，但是我们的数据却是非线性可分的。所以我们该怎么办？将数据投影到一个线性可分的空间，然后在那个空间寻找超平面！  
下面我们将逐步讲解这一想法。  
我们将上图中的数据投影到一个三维空间：  
&emsp;&emsp;$X_1=x_1^2$
&emsp;&emsp;$X_2=x_2^2$
&emsp;&emsp;$X_3=\sqrt{2}x_1{x_2}$
下面是投影到三维空间的数据。你是不是看到了一个可以悄悄放入一个平面的地方？  
![动画](img/gif1.gif)
让我们在其上运行SVM：  
![动画](img/gif2.gif)
太棒了！我们完美地分割了标签！现在让我们将这个平面投影到原本的二维空间：  
![图片](img/img8.jpg)
训练集上精确度100%，同时没有过于接近数据！  
原空间的分割边界的形状由投影决定。在投影空间中，分割边界总是一个超平面。  
别忘了，投影数据的主要目标是为了利用SVM寻找超平面的强大能力。  
映射回原始空间后，分割边界不再是线性的了。不过，我们关于线性分割、间隔、支持向量的直觉在投影空间仍然成立。  
![动画](img/gif3.gif)
我们可以看到，在左侧的投影空间中，三维的间隔是超平面之上的平面和之下的平面中间的区域（为了避免影响视觉效果，没有加上阴影），总共有4个支持向量，分别位于标识间隔的上平面和下平面。  
而在右侧的原始空间中，分割边界和间隔都不再是线性的了。支持向量仍然在间隔的边缘，但单从原始空间的二维情形来看，支持向量好像缺了几个。

### 算法内容

1. 线性可分支持向量机学习算法  
>+ 输入：线性可分训练数据集$T={(x_1,y_1),(x_2,y_2),...,(x_N,y_N)}$，其中，$x_i\in{R}^n,y_i\in{-1,+1},i=1,2,...,N$  

>+ 输出：最大间隔分离超平面和分类决策函数。  

>+ （1）构造并求解约束最优化问题：
$$\min\limits_{\omega,b} \frac{1}{2}\parallel\omega\parallel^2$$
&ensp;&emsp;&emsp;$s.t.$ 
&ensp;&emsp;&emsp;$y_i(\omega\cdot{x_i}+b)-1\geq0,i=1,2,...,N$
>+ （2）由此得到分离超平面：
$$\omega^\ast\cdot{x}+b^\ast=0$$  
分离决策函数：  
$$f(x)=sign(\omega^\ast\cdot{x}+b^\ast)$$

2. 线性可分支持向量机学习算法  
>+ 输入：线性可分训练数据集$T={(x_1,y_1),(x_2,y_2),...,(x_N,y_N)}$，其中，$x_i\in{R}^n,y_i\in{-1,+1},i=1,2,...,N$  

>+ 输出：最大间隔分离超平面和分类决策函数。  

>+ （1）构造并求解约束最优化问题：
$$\min\limits_{\alpha} \frac{1}{2}\sum_{i=1}^N\sum_{j=1}^N\alpha_i\alpha_j{y_i}{y_j}(x_i\cdot{x_j})-\sum_{i=1}^N\alpha_i$$
$s.t.$&emsp; $\sum_{i=1}^N\alpha_i{y_i}=0$ 
&emsp;&emsp;&emsp;$\alpha_i\geq0,i=1,2,...,N$  
求得最优解
$\alpha^\ast=(\alpha_1^\ast,\alpha_2^\ast,...,\alpha_N^\ast)^T$

>+ （2）计算
$$\omega^\ast=\sum_{i=1}^N\alpha_i^\ast{y_i}{x_i}$$  
并选择$\alpha^\ast$的一个正分量$\alpha_j>0$,计算
$$b^\ast=y_j-\sum_{i=1}^N\alpha_i^\ast{y_i}(x_i\cdot{x_j})$$

>+ （3）由此得到分离超平面：
$$\omega^\ast\cdot{x}+b^\ast=0$$  
分离决策函数：
$$f(x)=sign(\omega^\ast\cdot{x}+b^\ast)$$

3. 线性支持向量机学习算法  
>+ 输入：线性可分训练数据集$T={(x_1,y_1),(x_2,y_2),...,(x_N,y_N)}$，其中，$x_i\in{R}^n,y_i\in{-1,+1},i=1,2,...,N$  

>+ 输出：最大间隔分离超平面和分类决策函数。  

>+ （1）选择惩罚参数$C>0$,构造并求解凸二次规划问题：
$$\min\limits_{\alpha} \frac{1}{2}\sum_{i=1}^N\sum_{j=1}^N\alpha_i\alpha_j{y_i}{y_j}(x_i\cdot{x_j})-\sum_{i=1}^N\alpha_i$$
$s.t. \sum_{i=1}^N\alpha_i{y_i}=0$ 
$0\leq\alpha_i\leq{C},i=1,2,...,N$  
求得最优解$\alpha^\ast=(\alpha_1^\ast,\alpha_2^\ast,...,\alpha_N^\ast)^T$

>+ （2）计算
$$\omega^\ast=\sum_{i=1}^N\alpha_i^\ast{y_i}{x_i}$$  
选择$\alpha^\ast$的一个合适分量$0<\alpha_j^\ast<C$,计算
$$b^{\ast}=y_j-\sum_{i=1}^N\alpha_i^{\ast}y_i(x_i\cdot{x_j})$$

>+ （3）由此得到分离超平面：
$$\omega^\ast\cdot{x}+b^\ast=0 $$  
分离决策函数：
$$ f(x)=sign(\omega^\ast\cdot{x}+b^\ast) $$

4. 非线性支持向量机学习算法  
>+ 输入：线性可分训练数据集$T={(x_1,y_1),(x_2,y_2),...,(x_N,y_N)}$，其中，$x_i\in{R}^n,y_i\in{-1,+1},i=1,2,...,N$  

>+ 输出：分类决策函数。  

>+ （1）选择适当的核函数$K(x,z)$参数$C>0$,构造并求解最优化问题：
$$\min\limits_{\alpha} \frac{1}{2}\sum_{i=1}^N\sum_{j=1}^N\alpha_i\alpha_j{y_i}{y_j}K(x_i\cdot{x_j})-\sum_{i=1}^N\alpha_i$$
$s.t. \sum_{i=1}^N\alpha_i{y_i}=0$ 
$0\leq\alpha_i\leq{C},i=1,2,...,N$  
求得最优解$\alpha^\ast=(\alpha_1^\ast,\alpha_2^\ast,...,\alpha_N^\ast)^T$

>+ （2）计算
$$\omega^\ast=\sum_{i=1}^N\alpha_i^\ast{y_i}{x_i}$$  
选择$\alpha^\ast$的一个合适分量$0<\alpha_j^\ast<C$,计算
$$b^\ast=y_j-\sum_{i=1}^N\alpha_i^\ast{y_i}K(x_i,x_j)$$

>+ （3）构造决策函数：
$$f(x)=sign(\sum_{i=1}^N\alpha_i^\ast{y_i}K(x_i,x_j)+b^\ast)$$

### 线性可分支持向量机算法推导

SVM学习的基本想法是求解能够正确划分训练数据集并且几何间隔最大的分离超平面。如下图所示，$w{\cdot}x+b=0$即为分离超平面，对于线性可分的数据集来说，这样的超平面有无穷多个（即感知机），但是几何间隔最大的分离超平面却是唯一的。  
![分隔平面](img/img9.jpg)   
在推导之前，先给出一些定义。假设给定一个特征空间上的训练数据集  
$T=\{(x_1,y_1),(x_2,y_2),...,(x_N,y_N)\}$
其中，$x_i$$\in$$R$$^n$，$y_i$$\in$${+1,-1},i=1,2,...,N$，$x_i$为第$i$个特征向量，$y_i$为类标记，当它等于+1时为正例；为-1时为负例。再假设训练数据集是线性可分的。    
对于给定的数据集$T$和超平面$wx+b=0$，定义超平面关于样本点$(x_i,y_i)$的几何间隔为  
$$\gamma_i\in{y_i}(\frac{\omega}{\parallel\omega\parallel}\cdot{x_i}+\frac{b}{\parallel\omega\parallel})$$
超平面关于所有样本点的几何间隔的最小值为$$\gamma=\min\limits_{i=1,2,...,N}\gamma_i$$  
实际上这个距离就是我们所谓的支持向量到超平面的距离。
根据以上定义，SVM模型的求解最大分割超平面问题可以表示为以下约束最优化问题
$$\max\limits_{\omega,b}\gamma$$
&emsp;$s.t.$
&emsp;${y_i}(\frac{\omega}{\parallel\omega\parallel}\cdot{x_i}+\frac{b}{\parallel\omega\parallel})\geq\gamma,i=1,2,...,N$  
将约束条件两边同时除以$\gamma$，得到  
$${y_i}(\frac{\omega}{\parallel\omega\parallel\gamma}\cdot{x_i}+\frac{b}{\parallel\omega\parallel\gamma})\geq1$$
因为$\parallel\omega\parallel$，$\gamma$都是标量，所以为了表达式简洁起见，令
$$\omega=\frac{\omega}{\parallel\omega\parallel{\gamma}}$$
$$b=\frac{b}{\parallel\omega\parallel{\gamma}}$$
得到  
$$y_i(\omega\cdot{x_i}+b)\geq{1},i=1,2,...,N$$
又因为最大化$\gamma$，等价于最大化$\frac{1}{\parallel\omega\parallel}$，也就等价于最小化$\frac{1}{2}{\parallel\omega\parallel}^2$（$\frac{1}{2}$是为了后面求导以后形式简洁，不影响结果），因此SVM模型的求解最大分割超平面问题又可以表示为以下约束最优化问题
$$\min\limits_{\omega,b}\frac{1}{2}{\parallel\omega\parallel}^2$$
&emsp;$s.t.$  
&emsp;${y_i}(\frac{\omega}{\parallel\omega\parallel}\cdot{x_i}+\frac{b}{\parallel\omega\parallel})\geq{1},i=1,2,...,N$
这是一个含有不等式约束的凸二次规划问题，可以对其使用拉格朗日乘子法得到其对偶问题（dual problem）。  

首先，我们将有约束的原始目标函数转换为无约束的新构造的拉格朗日目标函数  
$$L(\omega,b,\alpha)=\frac{1}{2}{\parallel\omega\parallel}^2-\sum_{n=1}^N\alpha_{i}(y_i(\omega\cdot{x_i}+b)-1)$$
其中$\alpha_i$为拉格朗日乘子，且$\alpha_i\geq0$。现在我们令
$$\theta(\omega)=\max\limits_{\alpha_i\geq0}{L(\omega,b,\alpha)}$$
当样本点不满足约束条件时，即在可行解区域外：
$$y_i(\omega\cdot{x_i}+b)<1$$
此时，将$\alpha_i$设置为无穷大，则$\theta(\omega)$也为无穷大。
当满本点满足约束条件时，即在可行解区域内：
$$y_i(\omega\cdot{x_i}+b)\geq1$$
此时，$\theta(\omega)$为原函数本身。于是，将两种情况合并起来就可以得到我们新的目标函数
$$
\theta(\omega)=
\begin{cases}
\frac{1}{2}\parallel\omega\parallel^2,x\in可行域\\
\infty,  x\in不可行区域
\end{cases}
$$  
于是，原始问题就等价于  
$$\min\limits_{\omega,b}\theta(\omega)=\min\limits_{\omega,b}\max\limits_{\alpha_i\geq0}L(\omega,b,\alpha)=p^\ast$$  
看一下我们的新目标函数，先求最大值，再求最小值。这样的话，我们首先就要面对带有需要求解的参数$\omega$和$b$的方程，而$\alpha_i$又是不等式约束，这个方程不易求解。所以，需要使用拉格朗日函数对偶性，将最小和最大的位置交换一下，这样就变成了：  
$$\min\limits_{\omega,b}\theta(\omega)=\max\limits_{\alpha_i\geq0}\min\limits_{\omega,b}L(\omega,b,\alpha)=d^\ast$$   
要有$p^\ast=d^\ast$，需要满足两个条件:  
（1）优化问题是凸优化问题  
（2）满足KKT条件  
首先，本优化问题显然是一个凸优化问题，所以条件（1）满足，而要满足条件（2），即要求：  
$$
  \begin{cases}
  \alpha_i\geq0\\
  y_i(\omega_i\cdot{x_i}+b)-1\geq0\\
  \alpha_i(y_i(\omega_i\cdot{x_i}+b)-1)=0
\end{cases}
$$  
为了得到求解对偶问题的具体形式，令$L(\omega,b,\alpha)$对$\omega$和$b$的偏导为0，可得   
$$\omega=\sum_{i=1}^N\alpha_i{y_i}x_i$$  
$$\sum_{i=1}^N\alpha_i{y_i}=0$$  
将以上两个等式带入拉格朗日目标函数，消去$\omega$和$b$， 得   
$$L(\omega,b,\alpha)=\frac{1}{2}\sum_{i=1}^N\sum_{j=1}^N\alpha_i\alpha_j{y_i}y_j(x_i\cdot{x_j})-\sum_{i=1}^{N}\alpha_i{y_i}((\sum_{j=1}^{N}\alpha_j{y_j}x_j)\cdot{x_i}+b)+\sum{i=1}^{N}\alpha_i=-\frac{1}{2}\sum_{i=1}^{N}\sum_{j=1}^{N}\alpha_i\alpha_j{y_i}y_j(x_i\cdot{x_j})+\sum_{i=1}^{N}\alpha_i$$  
即 
$$\min\limits_{\omega ,b}L(\omega,b,\alpha)=-\frac{1}{2}\sum_{i=1}^{N}\sum_{j=1}^{N}\alpha_i\alpha_j{y_i}{y_j}(x_i\cdot{x_i})+\sum_{i=1}^{N}\alpha_i$$  
求$\min\limits_{\omega ,b}L(\omega,b,\alpha)$对$\alpha$的极大，即是对偶问题  
$$\max_\alpha-\frac{1}{2}\sum_{i=1}^{N}\sum_{j=1}{N}\alpha_i\alpha_j{y_i}{y_j}(x_i\cdot{x_j})+\sum_{i=1}^{N}\alpha_i$$  
&emsp;$s.t.$  
&emsp;$\sum_{i=1}^{N}\alpha_i{y_i}$  
&emsp;$\alpha_i\geq0,i=1,2,...,N$
把目标式子加一个负号，将求解极大转换为求解极小  
$$\min_\alpha-\frac{1}{2}\sum_{i=1}^{N}\sum_{j=1}{N}\alpha_i\alpha_j{y_i}{y_j}(x_i\cdot{x_j})-\sum_{i=1}^{N}\alpha_i$$  
&emsp;$s.t.$  
&emsp;$\sum_{i=1}^{N}\alpha_i{y_i}$  
&emsp;$\alpha_i\geq0,i=1,2,...,N$
现在我们的优化问题变成了如上的形式。对于这个问题，我们有更高效的优化算法，即序列最小优化（SMO）算法。  
我们通过这个优化算法能得到$\alpha^\ast$，再根据$\alpha^\ast$，我们就可以求解出$\omega$和$b$，进而求得我们最初的目的：找到超平面，即”决策平面”。  

前面的推导都是假设满足KKT条件下成立的，KKT条件如下：  
$$
  \begin{cases}
  \alpha_i\geq0\\
  y_i(\omega_i\cdot{x_i}+b)-1\geq0\\
  \alpha_i(y_i(\omega_i\cdot{x_i}+b)-1)=0
\end{cases}
$$
另外，根据前面的推导，还有下面两个式子成立
$$\omega=\sum_{i=1}^{N}\alpha_i{y_i}x_i$$
$$\sum_{i=1}^{N}\alpha_i{y_i}=0$$  
由此可知在$\alpha^\ast$中，至少存在一个$\alpha_j^\ast$（反证法可以证明，若全为0，则$\omega=0$，矛盾），对此j有  
$$y_j(\omega^\ast\cdot{x_j}+b^\ast)-1=0$$
因此可以得到  
$$\omega^\ast=\sum_{i=1}^{N}\alpha_i^\ast{y_i}x_i$$
$$b^\ast=y_j-\sum_{i=1}^{N}\alpha_i^\ast{y_i}(x_i\cdot{y_j})$$
对于任意训练样本$(x_i,y_i)$，总有$\alpha_i=0$或者$y_j(\omega\cdot{x_j}+b)=1$。若$\alpha_i=0$，则该样本不会在最后求解模型参数的式子中出现。若$\alpha_i>0$，则必有$y_i(\omega\cdot{x_j}+b=1)$，所对应的样本点位于最大间隔边界上，是一个支持向量。这显示出支持向量机的一个重要性质：训练完成后，大部分的训练样本都不需要保留，最终模型仅与支持向量有关。  

以上都是基于训练集数据线性可分的假设下进行的，但是实际情况下几乎不存在完全线性可分的数据，为了解决这个问题，引入了“软间隔”的概念，即允许某些点不满足约束  
$$y_i(\omega\cdot{x_j}+b)\geq1$$
采用hinge损失，将原优化问题改写为  
$$\min\limits_{\omega,b,\varepsilon_i}\frac{1}{2}\parallel\omega\parallel^2+C\sum_{i=1}^{m}\varepsilon_i$$
&emsp;$s.t.$  
&emsp;$y_i(\omega\cdot{x_i}+b)\geq1-\varepsilon_i$  
&emsp;$\varepsilon_i\geq0,i=1,2,...,N$  
其中$\varepsilon_i$为“松弛变量”，$\varepsilon_i=max(0,1-y_i(\omega\cdot{x_i}+b))$ ，即一个hinge损失函数。每一个样本都有一个对应的松弛变量，表征该样本不满足约束的程度。$C>0$称为惩罚参数，$C$值越大，对分类的惩罚越大。跟线性可分求解的思路一致，同样这里先用拉格朗日乘子法得到拉格朗日函数，再求其对偶问题。





### 参考资料

1. 《统计学习方法》（第2版）李航
2.  <a href='https://zhuanlan.zhihu.com/p/31886934'>支持向量机（SVM）——原理篇 - 知乎<a/>
3.  <a href="https://zhuanlan.zhihu.com/p/40857202">SVM教程：支持向量机的直观理解 - 知乎<a/> 



