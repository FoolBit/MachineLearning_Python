import pandas as pd 
from sklearn import cluster
import matplotlib.pyplot as plt 

data=pd.read_csv('../dataset/data.csv',encoding='gbk')
#print(data)
train_data=data[['2019年国际排名','2015亚洲杯']]
#print(train_data)
kmeans=cluster.KMeans(3)
result=kmeans.fit_predict(train_data)
data_new=pd.concat([pd.DataFrame(result),data['国家'],train_data],axis=1)
print(data_new)