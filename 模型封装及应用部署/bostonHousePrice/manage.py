import joblib
from flask import Flask, request, render_template
app = Flask(__name__)


@app.route('/', methods=['GET'])
def input_form():
    return render_template('form.html')


@app.route('/', methods=['POST'])
def input():
    r = request.form['rm']
    p = request.form['ptratio']
    l= request.form['lstat']
    rm = float(r)
    ptratio = float(p)
    lstat = float(l)
    if lr:
        # 回归预测
        inputData = [[rm, ptratio, lstat]]
        print(inputData)
        lr_y_predict = lr.predict(inputData)
        res = lr_y_predict[0][0]
        print(res)
        print('the model has been load')
    else:
        print('Train the model first')

    return render_template('forecastRes.html', forecast=res)

if __name__ == '__main__':
    lr = joblib.load('model.pkl')  # Load "model.pkl"
    print('Model loaded')
    app.run()