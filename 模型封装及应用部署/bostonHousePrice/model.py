from sklearn.datasets import load_boston
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression


dataset = load_boston()
x_data = dataset.data  # 导入所有特征变量
y_data = dataset.target  # 导入目标值（房价）

i_ = []
for i in range(len(y_data)):
    if y_data[i] == 50:
        i_.append(i)  # 存储房价等于50 的异常值下标
x_data = np.delete(x_data, i_, axis=0)  # 删除房价异常值数据
y_data = np.delete(y_data, i_, axis=0)  # 删除异常值
name_data = dataset.feature_names
j_ = []
for i in range(13):
    if name_data[i] == 'RM' or name_data[i] == 'PTRATIO' or name_data[i] == 'LSTAT':
        continue
    j_.append(i)  # 存储其他次要特征下标
x_data = np.delete(x_data, j_, axis=1)  # 在总特征中删除次要特征

# 随机选择20%的数据构建测试样本，剩余作为训练样本
X_train, X_test, y_train, y_test = train_test_split(x_data, y_data, random_state=0, test_size=0.20)

# 数据标归一化处理

# 分别初始化对特征和目标值的标准化器
min_max_scaler = preprocessing.MinMaxScaler()
# 分别对训练和测试数据的特征以及目标值进行标准化处理
X_train = min_max_scaler.fit_transform(X_train)
X_test = min_max_scaler.fit_transform(X_test)
y_train = min_max_scaler.fit_transform(y_train.reshape(-1, 1))  # reshape(-1,1)指将它转化为1列，行自动确定
y_test = min_max_scaler.fit_transform(y_test.reshape(-1, 1))  # reshape(-1,1)指将它转化为1列，行自动确定

# 使用线性回归模型LinearRegression对波士顿房价数据进行训练及预测
lr = LinearRegression()
# 使用训练数据进行参数估计
lr.fit(X_train, y_train)