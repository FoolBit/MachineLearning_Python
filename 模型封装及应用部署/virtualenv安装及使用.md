#### virtualenv安装及使用

---

若没有虚拟环境，所有第三方的包都会被pip安装在python的site-packages目录下,但有的时候不太应用的依赖包会需要不同版本的，virtualenv就是解决这个问题的，它可以建立一套独立的Python运行环境。

1. ```# pip install virtualenv```

2. ```# cd 项目的根目录```

   ```# virtualenv -p /usr/bin/python3 venv``` 新建一个python3的虚拟环境（/usr/bin/python3是刚刚安装的python3的路径；venv是虚拟环境的名字，可修改）

3. ```# source venv/bin/activate ``` 启动虚拟环境，启动后就可以看到命令提示符前有个(venv)