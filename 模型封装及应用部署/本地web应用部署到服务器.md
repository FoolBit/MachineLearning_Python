### 本地web项目部署到服务器(前置步骤：安装宝塔及apache/nginx，完成本地web应用)

---

#### 整体流程：

1. 打开本地应用，在pycharm的terminal中```pip freeze >requirements.txt ``` 导出所有的第三方包

2. 上传项目到服务器

3. [安装python3](https://gitee.com/OSABC/MachineLearning_Python/blob/master/%E6%A8%A1%E5%9E%8B%E5%B0%81%E8%A3%85%E5%8F%8A%E5%BA%94%E7%94%A8%E9%83%A8%E7%BD%B2/python3%E5%AE%89%E8%A3%85.md)

4. [安装virtualenv](https://gitee.com/OSABC/MachineLearning_Python/blob/master/%E6%A8%A1%E5%9E%8B%E5%B0%81%E8%A3%85%E5%8F%8A%E5%BA%94%E7%94%A8%E9%83%A8%E7%BD%B2/virtualenv%E5%AE%89%E8%A3%85%E5%8F%8A%E4%BD%BF%E7%94%A8.md)

5. 在该项目下启动虚拟环境

6. ```pip install -r requirements.txt``` 在该虚拟环境下安装所有程序的依赖模块

7. ```python manage.py``` 运行程序，查看IP及端口号

8. 宝塔界面：网站->添加站点(域名填服务器的公网地址)

9. 设置->配置文件

   apache 在文件中加上

   ```
   ProxyPass / http://127.0.0.1:5000/
   ProxyPassReverse /  http://127.0.0.1:5000/
   ```

   nginx 在文件中加上

   ```
   location / {
     	proxy_pass http://127.0.0.1:5000;
   }
   ```

10. 运行程序：```cd 项目根目录```，```# source venv/bin/activate ``` 启动虚拟环境，```python manage.py``` 运行程序


