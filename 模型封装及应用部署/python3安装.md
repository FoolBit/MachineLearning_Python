#### python3安装

---

1. ```# mkdir /usr/local/python3``` 在/usr/local/目录下建立python3文件夹

2. ```# cd /usr/local/python3``` 打开这个文件夹

3. ```# wget http://mirrors.sohu.com/python/3.7.4/Python-3.7.4.tgz``` 下载安装包

4. ```# tar -xvzf Python-3.7.4.tgz``` 解压

5. ```# cd Python-3.7.4``` 进入Python-3.7.4文件夹

6. ```# ./configure --prefix=/usr/local/python3``` 配置安装路径

7. ```# make && make install ``` 编译、安装可执行文件

8. ```# ln -s /usr/local/python3/bin/python3 /usr/bin/python3```

   ```# ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3```

   使用命令创建软连接

9. ```python3 -V```

   若显示版本等信息就为安装成功